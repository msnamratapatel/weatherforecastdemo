import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  //State
  state: {
    OpenWeatherAppId: '3a54121482b2c299cc384efe56b7e867',
    searchResult: {
      list: []
    },
    selectedLocation: {},
    searchText:"",
    weeklyForeCastDetails:[],
  },
  //Mutations
  mutations: {
    API_FAILURE: function (state, error) {
      console.log(error);
    },
    SEARCH_LOCATION: function (state, searchResult) {
      state.searchResult = searchResult;
    },
    WEEKLY_FORECAST:function(state,data){
      state.weeklyForeCastDetails = data;
    },
  },
  //Actions
  actions: {
    searchLocation: function ({
      commit,
      state
    }, city) {
      var params = {
        'q': city,
        'type': 'like',
        'sort': 'population',
        'cnt': 30,
        'units': 'metric',
        'appid': state.OpenWeatherAppId
      };

      var openWeatherApiSearchUrl = "http://api.openweathermap.org/data/2.5/find";

      return new Promise((resolve, reject) => {
        axios.get(openWeatherApiSearchUrl, {
            params: params
          }).then(response => {
            commit('SEARCH_LOCATION', response.data)
            resolve(response);
          })
          .catch(e => {
            console.log(e);
            commit('API_FAILURE', e)
            reject(e);
          });
      });
    },
    getWeeklyWeatherForecastDetails: function ({
      commit,
      state
    }, location) {
      var params = {
        'lat': location.lat,
        'lon': location.lon,
        'appid': state.OpenWeatherAppId,
        'sort': 'population',
        'cnt': 56,
        'units': 'metric'
      };

      var weeklyWeatherForecastApi = "http://api.openweathermap.org/data/2.5/forecast";

      return new Promise((resolve, reject) => {
        axios.get(weeklyWeatherForecastApi, {
            params: params
          }).then(response => {
            commit('WEEKLY_FORECAST', response.data)
            resolve(response);
          })
          .catch(e => {
            console.log(e);
            reject(e);
            commit('API_FAILURE', e)
          });
      });
    }
  }
});
