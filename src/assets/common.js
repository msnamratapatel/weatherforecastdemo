import Vue from 'vue'
import Snotify from 'vue-snotify';

Vue.use(Snotify);

export default{
  name:"notification",
  showError:function(messageTitle,messageBody){
    this.$snotify.error(messageBody, messageTitle, {
      timeout: 2000,
      showProgressBar: false,
      closeOnClick: false,
      pauseOnHover: true
    });
  }
};
