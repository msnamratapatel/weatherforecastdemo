import Vue from 'vue'
import Router from 'vue-router'
import SearchLocation from '@/components/Weather/SearchLocation'
import WeatherForecast from '@/components/Weather/WeatherForecast'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SearchLocation',
      component: SearchLocation
    },
    {
      path:'/weather-forecast',
      name:'WeatherForecast',
      component:WeatherForecast
    }
  ]
})
